# **Jeu du démineur par Goom12**

## **Description**
### Démineur
Le démineur est un jeu de réflexion dont le but est de localiser des bombes cachées sur une grille. 
La seule indication étant le nombre de bombes sur les huits cases adjacentes.     
En début de partie, tous les chiffres sont cachés. Les chiffres apparaissent en fonction des clics du joueur.  
Si le joueur clique sur un 0, toutes les cases adjacentes ainsi que tous les autres zeros autour sont dévoilés. Si le joueur clique sur un nombre, il est affiché. 
Si le joueur clique sur une bombe, il perd. S'il découvre toutes les cases sans cliquer sur une bombe, il gagne la partie.

### But du projet
Réaliser un démineur en javascript.

#### Contraintes imposées
- Faire de la programmation orientée objets.
- Utiliser la librairie P5.js pour faire une interface graphique un minimum jolie.
- Travailler sur un git, avec des descriptions précises du projet.
- Partionner le projet.

#### Outils utilisés
- Sublime text
- Chrome
- Gitlab

## **Démineur par Goom12**

### Arborescence
```
.                            
├── js                      Contient l'ensemble des fichiers js du projet                     
│   ├── main.js             Fichier principal d'éxécution                           
│   ├── variables.js        Contient l'ensemble des variables                  
│   ├── case.js             Contient la class objet (case)                   
│   ├── initialisation.js   Contient les fonctions d'initialisation            
│   └── fonctions.js        Contient les fonctions secondaire du projet                
│ 
├── index.html              Point d'exécution de la page est du jeu                  
└── readme.md               Documentation du projet      
```

### Utilisation
Pour lancer le jeu, il suffit de glisser le fichier "index.html" dans un navigateur internet.       
Pour commencer une partie, il faut cliquer sur une case à l'aide du clic gauche de la souris. Une case devient rouge lorsque la souris est dessus.     
Le premier clic de l'utilisateur dévoile forcément un ou plusieurs zeros, ainsi que les nombres adjacent.       
Le timer se met également en route.      
Une case grise foncée indique que la case est non-découverte, une case grise clair indique que la case à été découverte.    
Les numéros en couleur indiquent le nombre de mines aux alentours.       
Pour indiquer l'emplacement d'une bombe, on peut utiliser le clic droit. Un drapeau apparait alors et la case est vérouillée afin de ne pas la cliquer par erreur.
Pour pouvoir dévoiler cette case, il suffit de réutiliser le clic droit de la souris sur le drapeau.        
Lorsque l'emplacement d'une bombe est trouvée, le compteur de mine restante (en bas à droite) diminue.         
Une partie est perdue lorsque l'utilisateur a dévoilé une bombe. La partie est gagnée si l'utilisateur dévoile toutes les cases hormis les mines.          
A la fin de la partie, le timer est arrêté et un message de fin est affiché.

### Améliorations futures
- Ajouter 4 curseurs pour faire varier le nombre de mines, de cases en longueur, de cases en largeur, ainsi que la taille d'une case et la police d'écriture.
- Faire en sorte que le jeu soit redémarrable (sans raffraichir la page).
- Ajouter une animation de victoire et de défaite.

## **Spécificité technique**
Les fonctions du jeu seront décrites ici.
### initialisationTab
Cette fonction est appellée dès le lancement du jeu, elle crée le tableau contenant les cases d'objet. Le tableau est composé de deux dimensions. Chaque cases est un objet. 
### choixMines
Cette fonction est appellée après le premier clic gauche de l'utilisateur sur une case.    
Elle choisit les coordonnés d'une bombe de façon aléatoire. Si les coordonnés de la futur bombe correspondent à une des 8 cases autour du clic souris, d'autres coordonnés sont choisi. 
Il en va de même si la case est déjà une bombe. Dans le cas contraire, les coordonnés sont attribués à la bombe.
### valeurCase
Cette fonction est appellée après le premier clic gauche de l'utilisateur sur une case.         
Elle regarde chaques cases du tableau et calcule sa valeur en fonction du nombre de bombes dans les 8 cases aux alentours.
### info
Cette fonction est appellée toutes les 10 millisecondes, par la fonction draw.                  
A chaque appel de la fonction, la variable "timer" est recréée. Elle permet d'afficher la différence de temps entre le début de la partie et l'instant actuel. Cette différence de temps est ensuite affichée en bas à droite.        
La fonction affiche également le nombre de bombes restantes en fonction du nombre de bombes trouvées par l'utilisateur, en bas à droite.
### devoilerZeros
La fonction est appellée par la fonction objet "Case.cliked" si l'utilisateur a cliqué sur un zero.         
Avant l'appel à cette fonction, les coordonnés du zero sur lequel l'utilisateur à cliqué sont ajoutés dans un tableau tab0.     
La fonction est ensuite appellée. Cette fonction ouvre les 8 cases autour du zero dont les coordonés sont stockés dans la première case de tab0.         
Si on trouve un zero dans l'une des cases découvertes, alors on ajoute les coordonnés de la case dans tab0.       
On supprime ensuite la première case de tab0 car les coordonnés ont déjà été utilisés.         
La boucle se finit quand le tab0 est vide. 
Cette fonction ouvre les cases "en cascade". Plus la case est loin du zero initial, plus elle sera dévoilée tard.
### finDuJeu
Cette fonction est appellée toutes les 10 millisecondes, par la fonction draw.    
Elle vérifie l'état actuel du jeu afin d'afficher un message, et de finir le jeu.         
L'état de victoire est activé lorsque l'utilisateur a dévoilé toutes les cases sans ouvrir de bombe.       
L'état de défaite est activée lorsque l'utilisateur a cliqué sur une bombe.
### setup
Elle est utilisée qu'une seule fois, elle initialise le canvas, et bloque le menu contextuel du clic droit sur Chrome.
### draw
C'est la fonction principale du programme, elle est exécutée de façon automatique, toutes les 10 millisecondes.            
Elle incrémente des variables compteur, et exécute les autres fonctions.
### mousePressed
Cette fonction reçoit le clic souris de l'utilisateur et éxécute la fonction Case.clicked correspondant à l'objet.       
### mouseOver
Cette fonction regarde la position de la souris sur le canvas, et renvoit les coordonnés correspond à la fonction objet Case.over.

