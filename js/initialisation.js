function initialisationTab(){		//Initialise les cases du tableau avec les objets cases
	for (let i = 0; i < nombreCaseLongueur; ++i) {
		let row = [];
		for (let j = 0; j < nombreCaseLargeur; ++j) {
			row.push(new Case(i*tailleCase , j*tailleCase));
		}
		tabCase.push(row);
	}
}	

function choixMines(ystart,xstart){		//Les bombes sont définies 
	for(let i =0; i < nombreDeMines; i++){
		let imposible = false;
		let y = Math.floor(Math.random() * nombreCaseLongueur)+1;
		let x = Math.floor(Math.random() *(nombreCaseLargeur-1));
		if((y-1==ystart && x-1== xstart )|| (y-1==ystart && x== xstart )|| (y-1==ystart && x+1== xstart )||  (y==ystart && x-1== xstart )|| (y==ystart && x== xstart )||(y==ystart && x+1== xstart )|| (y+1==ystart && x-1== xstart )|| (y+1==ystart && x== xstart )|| (y+1==ystart && x+1== xstart ))
			imposible = true;		//Permet de vérifier que la future bombe est à une case minimum du premier clique souris
		if(!tabCase[y][x].bombe && !imposible){
			tabCase[y][x].bombe = true;
			tabCase[y][x].valeur = "💣";
		}
		else
			i--;
	}
}

function valeurCase(){		//Calcule la valeur de la case, si ce n'est pas une bombe, en fonction des bombes adjacentes à la case
	for (let i = 1; i < tabCase.length ; i++) {
		for (let j = 0; j < tabCase[1].length; j++) {
			if(!tabCase[i][j].bombe){	//Vérifie les 3 cases du haut
				let calculValeurCase = 0;
				if(i-1 >=1){
					if(j-1>=0)
						if(tabCase[i-1][j-1].bombe)
							calculValeurCase++;
					if(tabCase[i-1][j].bombe)
						calculValeurCase++;
					if(j+1<=nombreCaseLargeur-1)
						if(tabCase[i-1][j+1].bombe)
							calculValeurCase++;
				}
				if(j-1>=0)			//Vérifie les 2 cases autour
					if(tabCase[i][j-1].bombe)
						calculValeurCase++;
				if(j+1<=nombreCaseLargeur-1)
					if(tabCase[i][j+1].bombe)
						calculValeurCase++;

				if(i+1<=nombreCaseLongueur){	//Vérifie les 3 cases en dessous
					if(j-1>=0)
						if(tabCase[i+1][j-1].bombe)
							calculValeurCase++;
					if(tabCase[i+1][j].bombe)
						calculValeurCase++;
				if(j+1<=nombreCaseLargeur-1)
					if(tabCase[i+1][j+1].bombe)
						calculValeurCase++;
				}
				tabCase[i][j].valeur = calculValeurCase;	//Attribut la valeur à la case
			}
		}
	}
}